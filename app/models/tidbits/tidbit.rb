# frozen_string_literal: true

module Tidbits
  class Tidbit < ApplicationRecord
    def self.by_key(key)
      where(key: key.to_s.downcase).first_or_create
    end

    validates :key, presence: true, uniqueness: true
    has_rich_text :body

    def key
      self[:key].to_s.downcase
    end

    def key=(value)
      super(value.to_s.downcase)
    end
  end
end
