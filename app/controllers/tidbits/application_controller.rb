module Tidbits
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception

    def configured_before_action
      action = ::Tidbits.config.before_action
      return true unless action

      if action.respond_to?(:call)
        action.call
      else
        public_send(action)
      end
    end
  end
end
