require_dependency "tidbits/application_controller"

module Tidbits
  class TidbitsController < ApplicationController
    before_action :configured_before_action, except: :show
    before_action :find_tidbit, except: :index
    before_action :set_mode
    helper_method :previewing?

    def index
      @tidbits = Tidbit.all
    end

    def edit; end

    def update
      @tidbit.assign_attributes(tidbit_params)
      set_mode

      if previewing? || @tidbit.save
        render :show
      else
        @error = I18n.t(".error", default: "There was an error with your request. Please try again.")
        render :edit, status: :unprocessable_entity
      end
    rescue StandardError
      @error = I18n.t(".error", default: "There was an error with your request. Please try again.")
      render :edit, status: :internal_server_error
    end

    def show; end

    def previewing?
      params[:commit] == "Preview"
    end

    private

    def set_mode
      @plain = params[:plain] == "true"
    end

    def find_tidbit
      @tidbit = Tidbit.find(params[:id])
    end

    def tidbit_params
      params.require(:tidbit).permit(:body)
    end
  end
end
