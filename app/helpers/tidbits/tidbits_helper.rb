# frozen_string_literal: true

module Tidbits
  module TidbitsHelper
    def tidbit(key, editable: nil, plain: false, &block)
      editable = can_edit_tidbits? if editable.nil?

      tidbit = key.is_a?(Tidbit) ? key : Tidbit.by_key(key)
      render(tidbit, locals: { editable: editable, plain: plain }, &block)
    end

    def tidbits_form_container
      tag.turbo_frame "", id: "tidbit-form"
    end

    def can_edit_tidbits?
      action = ::Tidbits.config.can_edit
      return false unless action

      if action.respond_to?(:call)
        action.call
      else
        public_send(action)
      end
    end
  end
end
