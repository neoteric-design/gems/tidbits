# ⚠️ Archival Notice ⚠️ 

This project was a part of Neoteric Design's internal tooling and prototypes. 

These are no longer officially supported or maintained, and may contain bugs or be in any stage of (in)completeness.

This repository is provided as a courtesy to our former clients to support their projects going forward, as well in the interest of giving back what we have to the community. If you found yourself here, we hope you find it useful in some capacity ❤️ 


--------------------

# Tidbits
Add editable bits of content to any view

## Usage

Place a `tidbit` with a unique (or not unique if you'd like to re-use) identifier. Think url slugs. When this is rendered, the Tidbit corresponding to that key will be fetched or created and rendered. 

```erb
<%= tidbit "homepage-example-header" %>

```
You may also pass a block to serve as default content if the Tidbit has not been populated.

```erb
<%= tidbit "homepage-example-header" do %>
  <h1>Recent Blurbs</h1>
<% end %>
```

### Configuration
You can set an controller action to perform, for authentication purposes.
You can also set a method or proc to determine the default check to enable the edit link display.


```
Tidbits.configure do |config|
  config.before_action = :authenticate_admin_user!
  config.can_edit = :admin_user_signed_in?
end
```

To use live reloading, make sure you have Hotwire/Turbo enabled and

```erb
<%= tidbits_form_container %>

```
Somewhere on your page. The installer attempts to add this to `app/views/layouts/application.html.erb`


## Installation
Add this line to your application's Gemfile:

```ruby
gem 'tidbits'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install tidbits
```

Run the install generator

```bash
$ bin/rails g tidbits:install
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
