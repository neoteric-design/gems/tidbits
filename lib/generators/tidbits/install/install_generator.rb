module Tidbits
  class InstallGenerator < Rails::Generators::Base
    source_root File.expand_path("templates", __dir__)
    def copy_templates
      template "initializer.rb.erb",
               "config/initializers/tidbits.rb"
    end

    def setup_routes
      route "mount Tidbits::Engine => '/tidbits/'"
    end

    def install_migrations
      rake "tidbits:install:migrations"
    end

    def add_to_layout
      html = %(\n\n<%= tidbits_form_container %>\n)
      insert_into_file "app/views/layouts/application.html.erb", html, before: "</body>"
    end
  end
end
