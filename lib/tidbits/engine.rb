module Tidbits
  class Engine < ::Rails::Engine
    isolate_namespace Tidbits

    config.generators do |gen|
      gen.test_framework :rspec, view_specs: false
    end

    # config.to_prepare do
    #   ::ApplicationController.helper(Tidbits::TidbitsHelper)
    # end

    ActiveSupport.on_load(:action_controller_base) do
      ::ActionController::Base.helper(Tidbits::TidbitsHelper)
    end
  end
end
