# fronzen_string_literal: true

module Tidbits
  class << self
    attr_reader :config

    ##
    # Set configuration options
    #
    #   Tidbits.configure do |config|
    #     config.before_action = :authenticate_admin_user!
    #     config.can_edit = :admin_user_signed_in?
    #   end

    def configure
      @config = Configuration.new
      yield config
    end
  end

  class Configuration
    SETTINGS = %i[before_action can_edit].freeze
    attr_accessor(*SETTINGS)
  end
end
