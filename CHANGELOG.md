# Tidbits Changelog

## 0.1.2

* Fix assigned `before_action` not being called properly
* Make edit check pessimistic by default

## 0.1.1

* Style tweaks

## 0.1.0

* Initial release
* Assumes tailwind, hotwire, and trix config switching are in place (But works\* without them)

