require "rails_helper"

RSpec.describe "Tidbits show", type: :system do
  include ActionView::RecordIdentifier
  let!(:tidbit) { Tidbits::Tidbit.create(key: SecureRandom.hex) }
  before(:each) { perform_visit }
  let(:subject) { page }

  def perform_visit(params = {})
    visit tidbits.tidbit_path(tidbit, params)
  end

  it "renders tidbit" do
    is_expected.to have_selector("##{dom_id(tidbit)}")
  end

  it("resets form") { is_expected.to have_selector("turbo-stream[target='tidbit-form']") }

  context "editable" do
    before(:each) do
      enable_edits!
      perform_visit
    end

    it { is_expected.to have_link(href: tidbits.edit_tidbit_path(tidbit)) }
  end

  context "not editable" do
    before(:each) do
      disable_edits!
      perform_visit
    end
    it { is_expected.to_not have_link(href: tidbits.edit_tidbit_path(tidbit)) }
  end

  context "previewing" do
    before(:each) { perform_visit(commit: "Preview") }
    it { is_expected.to_not have_selector("turbo-stream[target='tidbit-form']") }
    it { is_expected.to_not have_link(href: tidbits.edit_tidbit_path(tidbit)) }
  end
end
