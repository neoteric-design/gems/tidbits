require "rails_helper"

RSpec.describe "Tidbits edit", type: :system do
  include ActionView::RecordIdentifier
  let(:tidbit) { Tidbits::Tidbit.by_key("example-one") }
  before(:each) do
    enable_edits!
    perform_visit
  end
  let(:subject) { page }

  def perform_visit
    visit "/static/page_one"
  end

  def activate_edit
    find_link(href: tidbits.edit_tidbit_path(tidbit)).click
  end

  def fill_in_body(value)
    find("input[name='tidbit[body]']", visible: :all).set(value)
  end

  it "renders form" do
    activate_edit
    is_expected.to have_selector("input[name='tidbit[body]']", visible: :all)
  end

  it "does edits" do
    activate_edit
    fill_in_body("Hi from the test zone")
    click_on "Save"

    within "##{dom_id(tidbit)}" do
      is_expected.to have_text("Hi from the test zone")
    end
  end

  it "does edits" do
    activate_edit

    fill_in_body("Hi from the test zone")
    click_on "Preview"

    within "##{dom_id(tidbit)}" do
      is_expected.to have_text("Hi from the test zone")
    end

    perform_visit

    is_expected.to_not have_text("Hi from the test zone")
  end
end
