require "rails_helper"

RSpec.describe "Tidbits index", type: :system do
  include ActionView::RecordIdentifier

  it "Displays listing of all tidbits" do
    10.times do |x|
      Tidbits::Tidbit.create!(key: "example-#{x}")
    end

    visit tidbits.tidbits_path

    expect(Tidbits::Tidbit.count).to eq(10)
    Tidbits::Tidbit.find_each do |tidbit|
      expect(page).to have_css("##{dom_id(tidbit)}")
      expect(page).to have_link(href: tidbits.edit_tidbit_path(tidbit))
    end
  end
end
