Rails.application.routes.draw do
  mount Tidbits::Engine => "/tidbits/"
  get "static/page_one"
  get "static/page_two"
end
