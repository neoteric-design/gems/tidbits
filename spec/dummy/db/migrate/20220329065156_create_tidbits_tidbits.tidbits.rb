# This migration comes from tidbits (originally 20220328222952)
class CreateTidbitsTidbits < ActiveRecord::Migration[6.1]
  def change
    create_table :tidbits_tidbits do |t|
      t.string :key
      t.timestamps
    end
    add_index :tidbits_tidbits, :key, unique: true
  end
end
