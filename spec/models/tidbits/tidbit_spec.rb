require "rails_helper"

module Tidbits
  RSpec.describe Tidbit, type: :model do
    it { is_expected.to respond_to(:key, :body) }

    describe "::by_key" do
      it "is case insensitive" do
        expect(Tidbit.by_key("Example-One")).to eq(Tidbit.by_key("example-one"))
      end

      it "creates if not found" do
        expect { Tidbit.by_key(SecureRandom.hex) }.to change(Tidbit, :count).from(0).to(1)
      end

      it "finds if exists" do
        existing = Tidbit.create(key: "foo")
        expect(Tidbit.by_key("foo")).to eq(existing)
      end
    end
  end
end
