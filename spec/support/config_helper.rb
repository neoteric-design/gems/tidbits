module ConfigHelper
  def enable_edits!
    Tidbits.config.can_edit = -> { true }
  end

  def disable_edits!
    Tidbits.config.can_edit = -> { false }
  end
end

RSpec.configure do |config|
  config.include(ConfigHelper)
end
