require "selenium-webdriver"

# Capybara.server = :puma, { Silent: true }
Capybara.server = :webrick

RSpec.configure do |config|
  config.before(:each, type: :system) do
    driven_by :rack_test
  end

  if ENV["SELENIUM_URL"].present?
    Capybara.server_host = "0.0.0.0"
    config.before(:each, type: :system, js: true) do
      ip = Socket.ip_address_list.detect { |addr| addr.ipv4_private? }.ip_address
      host! "http://#{ip}:#{Capybara.server_port}"

      driven_by :selenium, using: :firefox, screen_size: [1400, 1400],
                           options: { url: ENV["SELENIUM_URL"] }
    end
  else
    config.before(:each, type: :system, js: true) do
      driven_by :selenium, using: :headless_firefox, screen_size: [1400, 2400]
    end
  end

  # config.after(:each, type: :system, js: true, js_console: true) do
  #   errors = page.driver.browser.manage.logs.get(:browser)
  #   if errors.present?
  #     message = errors.map(&:message).join("\n")
  #     Rails.logger.error "** Javascript console messages *****"
  #     Rails.logger.error message
  #     Rails.logger.error "************************************"
  #   end
  # end
end
