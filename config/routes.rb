Tidbits::Engine.routes.draw do
  resources :tidbits, except: %(new create)
end
