require_relative "lib/tidbits/version"

Gem::Specification.new do |spec|
  spec.name        = "tidbits"
  spec.version     = Tidbits::VERSION
  spec.authors     = ["Madeline Cowie"]
  spec.email       = ["madeline@cowie.me"]
  spec.homepage    = "https://www.neotericdesign.com"
  spec.summary     = "Add editable lil tidbits to any view"
  spec.description = "Add editable lil tidbits to any view"
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  # spec.metadata['allowed_push_host'] = "http://rubygems.com"

  spec.metadata["homepage_uri"] = spec.homepage
  # spec.metadata['source_code_uri'] = "TODO: Put your gem's public repo URL here."
  # spec.metadata['changelog_uri'] = "TODO: Put your gem's CHANGELOG.md URL here."

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", ">= 6.1.5"

  spec.add_development_dependency "capybara"
  spec.add_development_dependency "rspec-rails"
  spec.add_development_dependency "selenium-webdriver"
end
